package mob;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("SecondTests")
@Tag("RegressionTests")
class SecondTests {

    @Test
    @DisplayName("Test one in SecondTests")
    void testOne() {}

    @Test
    @DisplayName("Test two in SecondTests")
    void testTwo() {}

    @Test
    @DisplayName("Test three in SecondTests")
    void testThree() {}

    @Test
    @DisplayName("Test four in SecondTests")
    void testFour() {}
}
