package mob;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("FirstTests")
@Tag("RegressionTests")
class FirstTests {

    @Test
    @DisplayName("Test one in SecondTests")
    void testOne() {}

    @Test
    @DisplayName("Test one in SecondTests")
    void testTwo() {}

    @Test
    @DisplayName("Test one in SecondTests")
    void testThree() {}

    @Test
    @DisplayName("Test one in SecondTests")
    void testFour() {}

    @Test
    @DisplayName("Test one in SecondTests")
    void testFive() {}
}
